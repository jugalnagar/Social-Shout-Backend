package com.Internity.dto;

import lombok.Data;

@Data
public class PostDTO {

    private String description;
    private String postPic;

}
